﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace D08Ramya.Models
{
    public class AppDbContext : DbContext
    {
        public DbSet<Game> Game { get; set; }
    }
}
