﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace D08Ramya.Models
{
    public class Game
    {
        [Required]
        [Display(Name = "GameID")]
        public int GameID { get; set; }

        [Display(Name = "Game name is:")]
        public string GameName { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Date not valid.")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$", ErrorMessage = "Date must be in format mm/dd/yyyy")]
        public DateTime GameDate { get; set; }

        [Display(Name = "Game details")]
        public string description { get; set; }

        
        public static List<Game> ReadAllFromCSV(string filepath)
        {
            List<Game> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Game.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Game OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Game item = new Game();

            int i = 0;

            item.GameName = Convert.ToString(values[i++]);
            item.GameDate = Convert.ToDateTime(values[i++]);
            item.description = Convert.ToString(values[i++]);
           
            return item;
        }
    }
}
