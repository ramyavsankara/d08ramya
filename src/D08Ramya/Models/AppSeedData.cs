﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace D08Ramya.Models
{
    public class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {

            string relPath = appPath + "/Models/SeedData/";
            var context = serviceProvider.GetService<AppDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }

            context.Game.RemoveRange(context.Game);
            context.SaveChanges();

            SeedGamesFromCsv(relPath, context);
        }

        private static void SeedGamesFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "Game_data.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Game.ReadAllFromCSV(source);
            List<Game> lst = Game.ReadAllFromCSV(source);
            context.Game.AddRange(lst.ToArray());
            context.SaveChanges();
        }

    }
}
